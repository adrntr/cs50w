from django.http import HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import render

from .models import Flight, Passenger


# Create your views here.
def index(request):
    return render(request, "flights/index.html", {
        "flights": Flight.objects.all()
    })


def flight(request, flight_id):
    flight = Flight.objects.get(pk=flight_id)
    return render(request, "flights/flight.html", {
        "flight": flight,
        "passengers": flight.passengers.all(),
        "non_passengers": Passenger.objects.exclude(flights=flight).all()
    })


def book(request, flight_id):
    """
    The post receives a passenger id
    The flight_id comes from the url -> <int:flight_id>/book
    :param request:
    :param flight_id:
    :return:
    """
    if request.method == "POST":
        flight = Flight.objects.get(pk=flight_id)
        passenger = Passenger.objects.get(pk=int(request.POST["passenger"]))

        passenger.flights.add(flight)

        # Reverse takes an url name and return the url.
        # example: path("<int:flight_id>", views.flight, name="flight")
        # by sending "flight" and the flight_id as arg, we get "<int:flight_id>"
        return HttpResponseRedirect(reverse("flight", args=(flight_id,)))


