"""
##########################################
JAVASCRIPT
##########################################
Javascript is the code used in the client side to avoid asking to the backend
and making it more fluid


In the HTML by adding this we are indicating that this is a javascript code
<script>
    alert('Hello, world!')
</script>

example:

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Hello</title>
        <script>
            alert('Hello, world!')
        </script>
    </head>
    <body>
    <h1>Hello</h1>
    </body>
    </html>

#########################################
# Events driven
#########################################
Allows to interact with the user based on the actions he/she takes

##ONCLICK
 Show an alert when a button click on the button

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Hello</title>
        <script>
            function hello(){
                alert('Hello, world!')
            }
        </script>
    </head>
    <body>
    <h1>Hello</h1>
    <button onclick="hello()">Click me</button>
    </body>
    </html>

#########################################
# Variables
#########################################
var -> used to define a variable globally
LET -> To create variables that is limited in scope to the current block
const -> To create const
let counter = 0;

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Hello</title>
        <script>
            let counter = 0;
            function count(){
                counter += 1;
                alert(counter)
            }
        </script>
    </head>
    <body>
    <h1>Hello</h1>
    <button onclick="count()">Count</button>
    </body>
    </html>

#########################################
# Query Selector
#########################################
document.querySelector allows to extract an element from the html and interact/use it
(querySelector only return one element)

    document.querySelector('h1').innerHTML = 'Goodbye';

This search the first h1 item and changes the innerHTML property to 'Goodbye'

With === we check the strict equality, same value and same type

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Hello</title>
        <script>
            function hello(){
                const heading = document.querySelector('h1');
                if(heading.innerHTML === 'Hello!') {
                    heading.innerHTML = 'Goodbye!';
                }else{
                    heading.innerHTML = 'Hello!';
                }
            }
        </script>
    </head>
    <body>
    <h1>Hello!</h1>
    <button onclick="hello()">Click me</button>
    </body>
    </html>

#########################################
# DOM Manipulation
#########################################
You can edit the items in the selector, for example a h1 with the counter value

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Hello</title>
        <script>
            let counter = 0;
            function count(){
                counter += 1;
                document.querySelector('h1').innerHTML = counter
            }
        </script>
    </head>
    <body>
    <h1>0</h1>
    <button onclick="count()">Count</button>
    </body>
    </html>

## Strings and variables

`value is ${value}`

## DOMContentLoaded
set count (NOT count()) to the onclick property of the button

PROBLEM: document.querySelector('button') do not find the button and return null, so onclick do not work

<script>
    function count(){
    ...
    }

    document.querySelector('button').onclick = count;
</script>

## EVENT LISTENER
To add an event listener to detect when all elements in the html are loaded to avoid having previous problem use:

    document.addEventListener('DOMContentLoaded', function(){
        document.querySelector('button').onclick = count;
        #this is the same
        # document.querySelector('button').addEventListener('click', count)
    });

The function addEventListener takes two arguments,
    - 1st: The event to listen (DOMContentLoaded)
    - 2nd: a function that can be declared directly in the argument, this function has no name (anonymous).
            this function will set the onclick function for the button

#############################################
## Move Script to a separate javascript file
#############################################

move all script code to a new file called counter.js

and in the html file indicate where this file is by

    <script src="counter.js"></script>

########################
# Query Selector
########################
if multiple items are from the same tag (i.e. h1) we can set and id
and get the item by id

    document.querySelector('tag') -> Gets the first element with the tag
    document.querySelector('#id') -> Gets the element with the id
    document.querySelector('.class') -> Gets the first element of the class

############################
# .onsubmit, .value
############################
- onsubmit: we can assign a function to the property .onsubmit of a form
- value: obtain the value of a item

    document.addEventListener('DOMContentLoaded', function(){
        document.querySelector('form').onsubmit = function(){
            const name = document.querySelector('#name').value;
            alert(`Hello, ${name}!`)
        }
    });

##############################
# Data for items
##############################
If we want to assign data for a item we can use

    data + - + key = "value"

For example, if we want to create 3 buttons that set the color
of another element, we can define the color in the dataset of each
button

<button data-color="red">Red</button>
<button data-color="blue">Blue</button>
<button data-color="yellow">Yellow</button>

###############
# QuerySelectorAll
###############
with querysSelectorAll we get an array of all elements that
meets the conditions

    document.querySelectorAll('button')

this return an array with the 3 buttons

#############
# access dataset
##############
to access the data-color of a item use dataset

    document.querySelector("#buttonid").dataset.color

#############
# For each
##############
array.forEach loop over an array and executes a function for each
one of its items

array.forEach(function(item){
    ...
});

########################
# Function -> => arrow
#########################
When calling a function from an argument of another function we can use
=> to simplify

arguments => function

this writting:

    document.addEventListener('DOMContentLoaded', function(){
        document.querySelectorAll('button').forEach(function(button){
            button.onclick = function(){
                document.querySelector('#hello').style.color = button.dataset.color
            }
        });
    });

can be replaced by:

    document.addEventListener('DOMContentLoaded',() => {
        document.querySelectorAll('button').forEach(button => {
            button.onclick = () => {
                document.querySelector('#hello').style.color = button.dataset.color
            }
        });
    });

No arguments
    () => {}
arguments
    button => {}


##############################
# OnChange and this
##############################
We can use in a selector an attribute onchange to execute actions
when the user change the option

this --> in the context of an onchage element, this will return the
element, in this case, the select

    document.querySelector('select').onchange = function(){
        document.querySelector('#hello').style.color = this.value
    }

####################################
# Other Events
################################

    - onclick
    - onmouseover
    - onkeydown
    - onkeyup
    - onload
    - onblur
    - ...

###############################################
# EVENTS --> See 08_tasks.html
###############################################

onsubmit context --> return False --> Avoid redirecting the page to another page (if you will do everything client side)

# document.createElement
    const li = document.createElement('li')

# append an element to another

    document.querySelector('#tasks').append(li)

# disable

    document.querySelector('#submit').disabled = true;

# onkeyup --> When press and leave a key

    document.querySelector('#task').onkeyup = () => {
        document.querySelector('#submit').disabled = false
    }

####################################################
#   Intervals
####################################################
Set an interval to execute an acction every x milliseconds

    setInterval(function, msec);

example: Increment the count every 1 sec

    let counter = 0;
    function count(){
        counter += 1;
        document.querySelector('h1').innerHTML = counter;

    }

    document.addEventListener('DOMContentLoaded', function(){
        document.querySelector('button').onclick = count;
        setInterval(count, 1000);
    });


############################################################
#   Local Storage
############################################################

Store information inside of the web browser and then use them

There are two functions

    - localStore.getItem(key)
    - localStore.setItem(key, value)

    
# Example: 

    # Check the storage exist and create it
    if(!localStorage.getItem('counter')){
        localStorage.setItem('counter', 0);
    }


    function count(){
        #use and save the store
        let counter = localStorage.getItem('counter')
        counter ++;
        document.querySelector('h1').innerHTML = counter;
        localStorage.setItem('counter',counter)
    }
    document.addEventListener('DOMContentLoaded', function(){
        #Start the page with the storage
        document.querySelector('button').onclick = count;
        document.querySelector('h1').innerHTML = localStorage.getItem('counter')
    });

#############################################
#   Objects
#############################################
An object in javascript is like a dictionary in
python

    let person = {
        first: 'Harry',
        second: 'Potter'
    };

    person.first --> Harry
    person[first] --> Harry

###################################################
#   API
###################################################
Communicate to a service to request something and 
get an answer


fetch --> make requests (async)
promise --> fetch return a promise, something that is not
    an inmediatly answer
.then() --> handle the response of the promise when it finish
.catch() --> Handle if there was an error


***
    .then(response => response.json())

    is the same as

    .then(response => {
        return response.json()
    })

"""


























