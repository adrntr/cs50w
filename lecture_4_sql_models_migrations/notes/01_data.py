"""
SQL

Data Management System
    - MySQL
    - PostgreSQL
    - SQLite
    - ...

MySQL and PostgreSQL are for big projects and usually are in a different server.
SQLite store all data in a file

########################################################
SQL TYPES
########################################################

- TEXT: strings
- NUMERIC: boolean, datetime, etc
- INTEGER: positive and negative numbers
- REAL: like floats
- BLOB: Binary large object (audio, files, etc)

####################################
MYSQL TYPES
####################################
    - Char(size): string where size is the exact number
    - VARCHAR(size): stringer where size is the maximum number of characters
    - SMALLINT: small integer
    - INT: integer
    - BIGINT: big integer
    - FLOAT: float
    - DOUBLE: big float
    - ...

#######################################
CREATE A TABLE
#######################################
CREATE TABLE table_name(
    column_name type constraints
)

CREATE TABLE flights(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    origin TEXT NOT NULL,
    destination TEXT NOT NULL,
    duration INTEGER NOT NULL
),

#######################################
CONSTRAINTS
#######################################
Allows to avoid invalid data in the table:

    - CHECK: check a condition for a value (example, a value in a range)
    - DEFAULT: if not value, use the default
    - NOT NULL: the column have to have a value
    - PRIMARY KEY: identifier of each row
    - UNIQUE: every value will be unique

#######################################
INSERT INTO A TABLE
#######################################
INSERT INTO table_name
    (column1, column2, columnN)
    (value1, value2, valueN)

INSERT INTO flights
    (origin, destination, duration)
    VALUE ("New York", "London", 415)

######################################
SELECT FROM A TABLE
######################################
SELECT * FROM flights                           --> select all columns all rows
SELECT origin, destination FROM flights         --> select columns origin and destination all rows
SELECT * FROM flights WHERE id = 3              --> select all columns and the row with id 3

######################################
SQLite PROMPT
######################################
touch .sql file                     --> touch flights.sql           --> creates a sql file
sqlite3 .sql file                   --> sqlite3 flights.sql         --> Open promt to work with the db
--> Create a table
.tables                             --> list all tables
--> Insert rows in the table
--> select rows from the table
.mode columns                   --> Clean print
.headers yes                   --> Show headers
--> Select rows and now are show pretty


sqlite> select * from flights;
id  origin    destination   duration
--  --------  ------------  --------
1   New York  Buenos Aires  3600
2   Madrid    Barcelona     350
3   Prage     Budapest      1900
4   New York  Santiago      230

########################################
SEARCH QUERIES
########################################
Use AND, OR, IN

LIKE "%a%"  --> % mean o or more characters --> it search if the column has an "a" in it

AVERAGE
COUNT
MAX
MIN
SUM

############################################
UPDATE
############################################
UPDATE flights
    SET duration= 430
    WHERE origin = "New York"
    AND destination = "London";

###########################################
DELETE
###########################################
DELETE FROM flights WHERE destination = "Tokio"

###########################################
OTHER CLAUSES
###########################################
- LIMIT
- ORDER BY
- GROUP BY
- HAVING (is used with group to say show the value if have more than x items)
- ...




"""

