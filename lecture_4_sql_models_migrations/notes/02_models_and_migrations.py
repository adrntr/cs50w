"""
###########################################
MODELS
###########################################
Models are a way of creating a python class that is going to represent data that I want
Django to store inside a database and django will know the sql to use to create/manipulate a table

1. Create a project

    django-admin startproject airline
    cd airline

2. Create an app for manage the flights

    python manage.py startapp flights

3. Add flights as installed app in airline settings


    INSTALLED_APPS = [
        'flights',
        'django.contrib.admin',
        ...
    ]

4. add flights.urls in airline.urls (remember to import include)

    from django.contrib import admin
    from django.urls import include, path

    urlpatterns = [
        path('admin/', admin.site.urls),
        path('flights/', include("flights.urls"))
    ]

5. Create file urls.py in app folder flights and add urlspatterns list

    from django.urls import path
    from . import views

    urlpatterns = [

    ]

6. Define the models in flights/models.py
(A model for each table)
The model contains the properties that I want to track

    class Flight(models.Model):
        origin = models.CharField(max_length=64)
        destination = models.CharField(max_length=64)
        duration = models.IntegerField()


###########################################
MIGRATIONS
###########################################
A way to tell django that there are new models or changes that must be used to update the database
Two steps:

    1. Create the migrations: set of instructions to the django how to manipulate the database
    2. Take the instructions and apply them to the database

7. Make the migration (command line)

    python manage.py makemigrations

This created a migration 0001_initial.py in flights/migrations folder
In this file there is a migration model that have the instructions of how to manipulate the table

8. Migrate the changes (command line)

    python manage.py migrate

this create the table -->   Applying flights.0001_initial... OK
it is stored in db.sqlite3

###########################################
SHELL
###########################################
python provide a way to manage the tables without sql syntax

    python manage.py shell

Import --> Create --> Save

# INSERT INTO

>> from flights.models import Flight
>> f = Flight(origin="New York", destination="London", duration=415)
>> f.save()

We don't need sql, with this method django is creating a new flight (insert into) in the table

# SELECT ALL

>> Flight.objects.all()
<QuerySet [<Flight: Flight object (1)>]>

The answer is not very descriptive, so we can add the method __str__ to the class (models.py)

    class Flight(models.Model):
        origin = models.CharField(max_length=64)
        destination = models.CharField(max_length=64)
        duration = models.IntegerField()

        def __str__(self):
            return f"{self.id}: {self.origin} to {self.destination}"

>> flights = Flight.objects.all()
>> flights
<QuerySet [<Flight: 1: New York to London>]>

# SELECT FIRST

>> flights = Flights.objects.all()
>> flight = flights.first()
>> flight
<Flight: 1: New York to London>

# ACCESS PROPERTIES

>> flight.id
1
>> flight.origin
'New York'
>> flight.destination
'London'
>> flight.duration
415

# LINK MODELS/TABLES BY REFERENCE KEYS
For example the field origin must not be a string, it should be another table with ids and origin names

    class Airport(models.Model):
        code = models.CharField(max_length=3)
        city = models.CharField(max_length=64)

        def __str__(self):
            return f"{self.city} ({self.code})"

to reference use:

    origin = models.ForeignKey(Airport, on_delete=models.CASCADE, related_name="departures")
    # reference origin and destination to the table Airport
    # on_delete=models.CASCADE means that if an item of Airport is deleted, the flight is deleted
    # related_name --> name for reverse relation -
    #                  if a have a airport how to get all the flights that have it as origin (departures)
    #                  (original relation -> which is the origin airport of a flight)


# MAKE MIGRATION OF THE CHANGES

1. MAKE MIGRATIONS
>> python manage.py makemigrations

Response:
    Migrations for 'flights':
  flights/migrations/0002_airport_alter_flight_destination_alter_flight_origin.py
    - Create model Airport
    - Alter field destination on flight
    - Alter field origin on flight

2. MIGRATE
>>python manage.py migrate

This applies the changes to the database

# USING THE DATABASE WITH SHELL
>> python manage.py shell

Import all data from models
    >> from flights.models import *

create and save Airports
    >> jfk = Airport(code="JFK", city="New York")
    >> jfk.save()
    >> lhr = Airport(code="LHR", city="London")
    >> lhr.save()
    >> cdg = Airport(code="CDG", city="Paris")
    >> cdg.save()
    >> nrt = Airport(code="NRT", city="Tokyo")
    >> nrt.save()

create a flight
    >> f = Flight(origin=jfk, destination=lhr, duration=415)
    >> f.save()

Access flight attributes
    >> f.origin
    <Airport: New York (JFK)>
    >> f.destination
    <Airport: London (LHR)>
    >> f.origin.city
    'New York'
    >> f.origin.code
    'JFK'

Access airport attributes (we are using the related_name "arrivals" definition)
    >> lhr.arrivals.all()
    <QuerySet [<Flight: 1: New York (JFK) to London (LHR)>]>

#################
# FILTER AND GET
#################

    >> from flights.models import *
Filter --> Returns a list with the matching results
    >> Airport.objects.filter(city="New York")
    <QuerySet [<Airport: New York (JFK)>]>

Filter and get the first --> return an object
    >> Airport.objects.filter(city="New York").first()
    <Airport: New York (JFK)>

Get (same as filter.first) --> return an object
    >> Airport.objects.get(city="New York")
    <Airport: New York (JFK)>

################
# Create a new flight with airports previously saved
################

>> jfk = Airport.objects.get(city="New York")
>> jfk
<Airport: New York (JFK)>
>> cdg = Airport.objects.get(city="Paris")
>> cdg
<Airport: Paris (CDG)>
>> f = Flight(origin=jfk, destination=cdg, duration=435)

#########################################
# Use the data from the database in python
#########################################
This will show a list of the elements in the table Flight

# VIEWS -> Flight.objects.all()

    from .models import Flight

    # Create your views here.
    def index(request):
        return render(request, "flights/index.html", {
             "flights": Flight.objects.all()
        })

# HTML -> flight.id
    {% extends "flights/layout.html" %}

    {% block body %}
        <h1>Flights</h1>
        <ul>
            {% for flight in flights %}
                <li>Flight {{ flight.id }}: {{flight.origin}} to {{ flight.destination }}</li>
            {% endfor %}
        </ul>
    {% endblock %}
"""

"""
#########################################
DJANGO ADMIN TOOL
#########################################
In paths we see that there is a default one for /admin

    urlpatterns = [
        path('admin/', admin.site.urls),
        path('flights/', include("flights.urls"))
    ]

This is used to avoid using the shell to create new inputs in our tables

- Steps to use the django admin tool:

    1. Create a super user
        >> python manage.py createsuperuser
    2. Register the models in admin.py
        from .models import Flight, Airport
        # Register your models here.
        
        admin.site.register(Airport)
        admin.site.register(Flight)
    3. Go to /admin page --> http://127.0.0.1:8000/admin/
    4. Add flights and Airports using the admin interface
    5. Check that the flights are added in /flights
"""

"""
#########################################
MANY TO MANY RELATIONSHIPS
#########################################
Lets create a new model for passenger. In this case there is a relation many to many because a flight have multiple
passengers and a passenger can have multiple flights

    class Passenger(models.Model):
        first = models.CharField(max_length=64)
        last = models.CharField(max_length=64)
        flights = models.ManyToManyField(Flight, blank=True, related_name="passengers") 
            # blank: indicate if the passenger can not have flights
            # related_name: way to access all passengers that have a flight, (it is not needed to create passengers
            variable in Flight model)

Because of the new model we need to do migrations

    -> pyhon manage.py makemigrations
    -> python manage.py migrate
    
#
# Register the passenger model in admin.py
#

    admin.site.register(Passenger)
    
    
Now we can manage passengers from admin page

    -> python manage.py runserver

Go to /admin and add a passenger and select one or more flights for this user

#
# Show passengers information in flight page
#

First pass passengers as context

    def flight(request, flight_id):
        flight = Flight.objects.get(pk=flight_id)
        return render(request, "flights/flight.html", {
            "flight": flight,
            "passengers": flight.passengers.all() #we are using related_name from models
        })

Then show the list of the passengers

In flight.html add:

    <h2>Passengers</h2>
    <ul>
        {% for passenger in passengers %}
            <li>{{ passenger }}</li>
        {% empty %}
            <li>No passengers.</li>
        {% endfor %}
    </ul>

now in /flights/<id> the passengers are shown

#
# Making links in the html
#

- Back to prev page in flights.html
    <a href="{% url 'index' %}">Back to Flight List</a>
    
- add link to the index page where the list of flights are shown

    <a href="{% url 'flights' flight.id %}">  
        <li>Flight {{ flight.id }}: {{flight.origin}} to {{ flight.destination }}</li>
    </a>
    
    #Go to the url flight and pass the flight.id as argument.

#
# Creating Book option
#

Create the url in urls.py

    urlpatterns = [
        ....
        path("<int:flight_id>/book", views.book, name="book")
    ]
    
Create the view book

    
    def book(request, flight_id):
        if request.method == "POST":
            flight = Flight.objects.get(pk=flight_id)
            passenger = Passenger.objects.get(pk=int(request.POST["passenger"]))
            passenger.flights.add(flight)        
            return HttpResponseRedirect(reverse("flight", args=(flight_id,)))
            
Create a form in flight.html

    <form action="{% url 'book' flight.id %}" method="post">
        {% csrf_token %}
        <select name="passenger">
            {% for passenger in non_passengers %}
                <option value="{{ passenger.id }}">{{ passenger }}</option>
            {% endfor %}
        </select>
        <input type="submit">
    </form>

"""


##############################
# Custom Admin Page
##############################
"""
We can adapt the admin page

In the file admin.py you can do multiple changes like:

1. Add more information about a model, in this case we are showing id, origin, destination and duration

class FlightAdmin(admin.ModelAdmin):
    list_display = ("id", "origin", "destination", "duration")

admin.site.register(Flight, FlightAdmin)

2. Show the flights of a user in two selectors 

class PassengerAdmin(admin.ModelAdmin):
    filter_horizontal = ("flights",)

"""

##############################
# Users
##############################

"""
Create a new app for users

1- Create the app
    
    >> python manage.py startapp users

2- settings.py --> add 'users'

    INSTALLED_APPS = [
        'flights',
        'users',
        ...
    ] 

3- add users to urlpatters in url.py

    urlpatterns = [
        path('admin/', admin.site.urls),
        path('flights/', include("flights.urls")),
        path('users/', include("users.urls"))
    ]
    
4- in users folder, create a new file url.py and add the url patterns

    from django.urls import path
    from . import views
    
    urlpatterns = [
        path("", views.index, name="index"),
        path("login", views.login_view, name="login"),
        path("logout", views.logout_view, name="logout")
    ]
    
5- Write the functions for views.index, login_view and logout_view
Index --> Check if the user is logged, otherwise redirect to login page
login --> show login page
    
    
    def index(request):
    if not request.user.is_authenticated:
        # If the user is not authenticated redirect to the login view
        return HttpResponseRedirect(reverse("login"))


    def login_view(request):
        return render(request, "users/login.html")


6- Create the login.html page

    
"""