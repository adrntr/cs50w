from django.shortcuts import render
from django.http import HttpResponse


# Create your views here.

def index(request):
    """
    Only return a http response of "hello, world"
    :param request: represent the HTTP request that the user made in order to access our server
    :return:
    """
    return render(request, "hello/index.html")  # this folder is in hello/templates/hello/index.html


def brian(request):
    return HttpResponse("Hello, Brian!")


def david(request):
    return HttpResponse("Hello, David!")


# def greet(request, name):
#     """
#     this will be executed when calling /hello/<name>
#     :param request:
#     :param name: name of the person to greet
#     :return:
#     """
#     return HttpResponse(f"Hello, {name.capitalize()}")

def greet(request, name):
    """
    We use the template hello/greet.html in the folder hello/templates/hello and pass it a context with variables
    that the html file can use with {{ }}
    :param request:
    :param name:
    :return:
    """
    return render(request, "hello/greet.html", {
        "name": name.capitalize()
    })

