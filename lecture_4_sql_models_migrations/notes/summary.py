"""
1. Create project
2. create app (flights)
3. add app into INSTALLED_APPS in project/setting.py
4. add app into urlpatters in project/urls.py
5. Create the urls.py file in the app folder
6. Define the models in app/models.py
    Use ForeignKey for many-to-one relations
    User ManyToManyField for many-to-many relations
7. Do the migrations with makemigrations and migrate
8. Create views to add/see/edit the data
9. Add the views to app/urls.py in urlpatterns list
10. Create the htmls to show the data (inside app/templates/app)
/ADMIN
11. Register models in admin.py
12. Create data using /admin page

TODO: add booking to flight

###########################
# Data types
###########################
String      -> models.CharField(max_length=64)
Integer     -> models.IntegerField()


###########################
Data manipulation
###########################

from .models import Flight

##READ
    # Objects all
    - Flight.objects.all() -> Get all elements
    # Objects get
    - Flight.objects.get(pk=23) -> Get the flight which primary key is 23
    # Objects Exclude
    - Passenger.objects.exclude(flights=flight).all()

    - flight.id --> Get an attribute


    #Filter
    - comments = Comment.objects.filter(auction=auction, user=user)

#WRITE
    -   passenger.flights.add(flight_object)
    -   new_auction = Auction(title=title, description=description, starting_bid=initial_bid, picture=photo_url)
        new_auction.save()



###########################
HTML PYTHON
###########################

# VARIABLE:
    <li>{{ passenger.first }}</li>

## FOR
    {% for passenger in passengers %}
    {% empty %}
    {% endfor %}

## URL
    <a href="{% 'url_name' argument_if_any %}>text</a>
    <a href="{% url 'flight' flight.id %}">Back to flight lists</a>

## SELECT
    <select name="passenger">
        {% for passenger in passengers %}
            <option value= "{{ passenger.id }}"> {{ passenger }} </option>
        {% endfor %}
    </select>

    The select name is used to retrive the selected item in the view
    - request.POST["passenger"]
## FORM
    <form action="{% url 'view_name' argument_if_any}" method="get/post"></form>
    i.e.
    <form action="{% url 'book' flight.id}" method="post"></form>


###########################
Views and Urls - Arguments
###########################
URL:

urlpatterns = [
    path("<int:flight_id>", views.flight, name="flight") --> if an int is written
        in the url, it is defined as fligth_id and open the view.flight
]

VIEW:

flight_id is received as argument and used to the the flight as context

def flight(request, flight_id):
    flight = Flight.objects.get(pk=flight_id)
    return render(request, f"flights/{id}", context={
        "flight": flight
    })

HTML:

Use {% url 'flight' flight.id %} --> To indicate the page to redirect
    This goes to the view name=flight and send the argument flight.id

    <a href="{% url 'flight' flight.id %}">
    Flight {{ flight.id }}: {{ flight.origin }} to {{ flight.destination }}
    </a>


VIEW FOR POST METHOD:
    use
    - return HttpResponseRedirect(reverse("flight", args=(flight_id,)))
    to redirect to a view

        reverse takes a url name and a tuple of arguments and
        return a url

####################################
# FORMS
####################################

## Create class that inherits from forms.Form that represent your form

[views.py]
class NewAuctionForm(forms.Form):
    title = forms.CharField(label="Title")
    description = forms.CharField(label="Description")
    initial_bid = forms.IntegerField(label="Initial Bid", min_value=0)
    photo_url = forms.URLField(label="Photo URL")

## Render the page and pass the form as context

def create_auction(request):
    return render(request, "auctions/create.html", {
        "form": NewAuctionForm()
    })

### Use your form in the html
        {{ form.non_field_errors }}
        {% for field in form %}
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">{{ field.label }}</label>
                <div class="col-sm-10">
                    {{ field }}
                </div>
                <div class="col-sm">
                    {{ field.errors }}
                </div>
            </div>
        {% endfor %}

    form.non_field_errors --> show all errores not related with a field
    loop over each of the fields of the form (title, descr, et)
    Create a group for each label + field + error

"""
