import os
import random

import markdown
from django.shortcuts import render
from . import util
from django import forms
from django.http import HttpResponseRedirect
from django.urls import reverse


class NewPageForm(forms.Form):
    title = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Page Title'})
    )
    content_markdown = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Add a Markdown'})
    )


class EditPageForm(forms.Form):
    content_markdown = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Add a Markdown'}),
        initial="This is a predefined text"
    )


def index(request):
    return render(request, "encyclopedia/index.html", {
        "entries": util.list_entries()
    })


def entry(request, entry_name):
    entry_markdown = util.get_entry(entry_name)
    entry_html = ""
    if entry_markdown:
        entry_html = markdown.markdown(entry_markdown)

    return render(request, "encyclopedia/entry.html", {
        "entry_html": entry_html,
        "entry_name": entry_name
    })


def search(request):
    query = request.POST.get('query', '')
    available_entries = util.list_entries()
    # The search matches one of the wikipedia pages
    if query in available_entries:
        return entry(request, query)
    # The search is a substring of one or more of the wikipedia pages
    possible_entries = [available_entry for available_entry in available_entries if query in available_entry]
    return render(request, "encyclopedia/search.html", {
        "entries": possible_entries
    })


def new_page(request):
    if request.method == "POST":
        form = NewPageForm(request.POST)
        # Form is not valid
        if not form.is_valid():
            return render(request, "encyclopedia:new_page.html", {
                "form": form
            })
        title = form.cleaned_data["title"]
        # Page already exist
        if title in util.list_entries():
            form.add_error("title", "This page already exist")
            return render(request, "encyclopedia/new_page.html", {
                "form": form
            })
        # Create page
        content_markdown = form.cleaned_data["content_markdown"]
        util.save_entry(title, content_markdown)
        return HttpResponseRedirect(reverse(f"encyclopedia:entry", kwargs={'entry_name': title}))

    return render(request, "encyclopedia/new_page.html", context={
        "form": NewPageForm()
    })


def edit_page(request, entry_name):
    if request.method == "POST":
        form = EditPageForm(request.POST)
        if not form.is_valid():
            return render(request, "encyclopedia/edit_page.html", {
                "form": form,
                "entry_name": entry_name
            })
        content_markdown = form.cleaned_data["content_markdown"]
        util.save_entry(entry_name, content_markdown)
        return HttpResponseRedirect(reverse(f"encyclopedia:entry", kwargs={'entry_name': entry_name}))

    entry_content = util.get_entry(entry_name)
    edit_entry_form = EditPageForm()
    edit_entry_form.fields["content_markdown"].initial = entry_content
    return render(request, "encyclopedia/edit_page.html", {
        "form": edit_entry_form,
        "entry_name": entry_name
    })


def random_page(request):
    entries = util.list_entries()
    random_entry = random.choice(entries)
    return HttpResponseRedirect(reverse(f"encyclopedia:entry", kwargs={'entry_name': random_entry}))
