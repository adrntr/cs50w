from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.db import IntegrityError
from django import forms
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from .models import User, Auction, Watchlist, Comment, Bid, Category


class NewAuctionForm(forms.Form):
    """
    Form to create a new Auction
    """

    title = forms.CharField(label="Title", widget=forms.TextInput(attrs={'class': 'form-control'}))
    description = forms.CharField(label="Description",
                                  widget=forms.Textarea(attrs={'rows': 3, 'class': 'form-control'}))
    initial_bid = forms.IntegerField(label="Initial Bid", min_value=0,
                                     widget=forms.NumberInput(attrs={'class': 'form-control'}))
    photo_url = forms.URLField(label="Photo URL", required=False,
                               widget=forms.URLInput(attrs={'class': 'form-control'}))
    categories_choices = [(category.id, category.name) for category in Category.objects.all()]
    categories_choices.insert(0, ('', '---------'))
    category = forms.ChoiceField(label="Category", choices=categories_choices,
                                 widget=forms.Select(attrs={'class': 'form-control'}), required=False)


class NewCommentForm(forms.Form):
    """
    Form to create a new comment for an auction
    """
    text = forms.CharField(label="Comments", widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 3}))


class NewBidForm(forms.Form):
    """
    Form to post a new bid for an auction
    """
    bid = forms.IntegerField(label="Bid", min_value=0)


def login_view(request):
    """
    View to log in
    """
    if request.method == "POST":

        # Attempt to sign user in
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)

        # Check if authentication successful
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(reverse("auctions:index"))
        else:
            return render(request, "auctions/login.html", {
                "message": "Invalid username and/or password."
            })
    else:
        return render(request, "auctions/login.html")


def logout_view(request):
    """
    View to log out
    """
    logout(request)
    return HttpResponseRedirect(reverse("auctions:index"))


def register(request):
    """
    View to register as user
    """
    if request.method == "POST":
        username = request.POST["username"]
        email = request.POST["email"]

        # Ensure password matches confirmation
        password = request.POST["password"]
        confirmation = request.POST["confirmation"]
        if password != confirmation:
            return render(request, "auctions/register.html", {
                "message": "Passwords must match."
            })

        # Attempt to create new user
        try:
            user = User.objects.create_user(username, email, password)
            user.save()
        except IntegrityError:
            return render(request, "auctions/register.html", {
                "message": "Username already taken."
            })
        login(request, user)
        return HttpResponseRedirect(reverse("auctions:index"))
    else:
        return render(request, "auctions/register.html")


@login_required
def index(request):
    """
    View to show the active listing
    """
    return render(request, "auctions/index.html", {
        "auctions": Auction.objects.filter(active=True),
    })


@login_required
def create(request):
    """
    View to create a new listing
    """
    if request.method == "POST":
        form = NewAuctionForm(request.POST)
        if form.is_valid():
            title = request.POST.get("title")
            description = request.POST.get("description")
            initial_bid = request.POST.get("initial_bid")
            photo_url = request.POST.get("photo_url")
            category_id = request.POST.get("category")
            category = Category.objects.get(pk=category_id)
            new_auction = Auction(title=title,
                                  description=description,
                                  starting_bid=initial_bid,
                                  picture=photo_url,
                                  user=request.user,
                                  category=category)
            new_auction.save()

            new_bid = Bid(price=initial_bid, auction=new_auction, user=request.user)
            new_bid.save()

            return HttpResponseRedirect(reverse("auctions:auction", args=(new_auction.pk,)))
        else:
            return render(request, "auctions/create.html", {
                "form": form
            })

    return render(request, "auctions/create.html", {
        "form": NewAuctionForm(),
    })


@login_required
def close(request, auction_id):
    """
    View to close an auction
    """
    if request.method == "POST":
        auction = Auction.objects.get(pk=auction_id)
        if request.user == auction.user:
            auction.active = False

        return HttpResponseRedirect(reverse("auctions:auction", args=(auction_id,)))


@login_required
def auction(request, auction_id):
    """
    View to show an action or handle post request as new comment or new bid offer
    """
    comment_form = NewCommentForm()
    bid_form = NewBidForm()
    auction_item = Auction.objects.get(pk=auction_id)
    if request.method == "POST":
        if 'comment_form' in request.POST:
            comment_form = NewCommentForm(request.POST)
            if comment_form.is_valid():
                comment_text = request.POST["text"]
                new_comment = Comment(text=comment_text,
                                      user=request.user,
                                      auction=auction_item)
                new_comment.save()
                comment_form = NewCommentForm()
        if 'bid_form' in request.POST:
            bid_form = NewBidForm(request.POST)
            if bid_form.is_valid():
                auction_bids = auction_item.bids.all()
                higher_bid = max(auction_bids, key=lambda bid: bid.price)
                higher_bid_price = higher_bid.price
                new_bid_price = int(request.POST["bid"])
                if higher_bid_price < new_bid_price:
                    new_bid = Bid(price=new_bid_price,
                                  auction=Auction.objects.get(pk=auction_id),
                                  user=request.user)
                    new_bid.save()
                    bid_form = NewBidForm()
                    add_watchlist(request, auction_id=auction_id)
                else:
                    bid_form.add_error("bid", f"The bid must be higher than {higher_bid_price}")

        if 'close_form' in request.POST:
            if auction_item.user == request.user:
                auction_item.active = False
                auction_item.save()

    user_watchlist = Watchlist.objects.get(pk=request.user)
    auction = Auction.objects.get(pk=auction_id)
    in_watchlist = auction in user_watchlist.auctions.all()
    comments = Comment.objects.filter(auction=auction).order_by("-date")
    categories = Category.objects.all()
    return render(request, "auctions/auction.html", {
        "auction": auction,
        "n_bids": len(auction.bids.all()) - 1,
        "actual_bid": auction.actual_bid,
        "in_watchlist": in_watchlist,
        "comment_form": comment_form,
        "comments": comments,
        "bid_form": bid_form,
        "categories": categories,
    })


@login_required
def watchlist(request):
    """
    View to show the auctions added to the watchlist for a user
    """
    return render(request, "auctions/watchlist.html", {
        "watchlist": request.user.watchlist.auctions.all(),
    })


@login_required
def add_watchlist(request, auction_id):
    """
    Voew to add an auction into the watchlist
    :param request:
    :param auction_id:
    :return:
    """
    auction = Auction.objects.get(pk=auction_id)
    user_watchlist = request.user.watchlist.auctions.all()
    if auction in user_watchlist:
        request.user.watchlist.auctions.remove(auction)
    else:
        request.user.watchlist.auctions.add(auction)
    return HttpResponseRedirect(reverse("auctions:watchlist"))


@login_required
def categories(request):
    """
    View to show the available categories
    """
    return render(request, "auctions/categories.html", context={
        "categories": Category.objects.all()
    })


@login_required
def category(request, category_id):
    """

    :param request:
    :param category_id:
    :return:
    """
    category = Category.objects.get(pk=category_id)
    return render(request, "auctions/index.html", context={
        "auctions": Auction.objects.filter(category=category, active=True),
        "category": category
    })
