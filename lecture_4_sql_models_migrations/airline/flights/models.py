from django.db import models


class Airport(models.Model):
    code = models.CharField(max_length=3)
    city = models.CharField(max_length=64)

    def __str__(self):
        return f"{self.city} ({self.code})"


# Create your models here.
class Flight(models.Model):
    # reference origin and destination to the table Airport
    # on_delete=models.CASCADE means that if an item of Airport is deleted, the flight is deleted
    # related_name --> name for reverse relation -
    #                  if a have a airport how to get all the flights that have it as origin (departures)
    #                  (original relation -> which is the origin airport of a flight
    origin = models.ForeignKey(Airport, on_delete=models.CASCADE, related_name="departures")
    destination = models.ForeignKey(Airport, on_delete=models.CASCADE, related_name="arrivals")
    duration = models.IntegerField()

    def __str__(self):
        return f"{self.id}: {self.origin} to {self.destination}"

    def is_valid_flight(self):
        return self.duration > 0 and self.origin != self.destination


class Passenger(models.Model):
    first = models.CharField(max_length=64)
    last = models.CharField(max_length=64)
    flights = models.ManyToManyField(Flight, blank=True, related_name="passengers")

    def __str__(self):
        return f"{self.first} {self.last}"
