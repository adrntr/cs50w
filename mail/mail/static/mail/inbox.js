document.addEventListener('DOMContentLoaded', function () {


    // Use buttons to toggle between views
    document.querySelector('#inbox').addEventListener('click', () => load_mailbox('inbox'));
    document.querySelector('#sent').addEventListener('click', () => load_mailbox('sent'));
    document.querySelector('#archived').addEventListener('click', () => load_mailbox('archive'));
    document.querySelector('#compose').addEventListener('click', () => compose_email());

    // By default, load the inbox
    load_mailbox('inbox');

    document.querySelector('#compose-form').onsubmit = () => sendEmail();
});

/**
 * Function to show the screen to compose a new mail
 * @param recipients emails to send the message split by comma
 * @param subject topic of the message
 * @param body message of the mail
 */
function compose_email(recipients = '', subject = '', body = '') {

    // Show compose view and hide other views
    document.querySelector('#emails-view').style.display = 'none';
    document.querySelector('#compose-view').style.display = 'block';
    document.querySelector('#email-view').style.display = 'none';

    // Clear out composition fields
    document.querySelector('#compose-recipients').value = recipients;
    document.querySelector('#compose-subject').value = subject;
    document.querySelector('#compose-body').value = body;
}

/**
 * Show one of the mailboxes
 * Allowed mailbox:
 *  - inbox
 *  - sent
 *  - archived
 * @param mailbox name of the type of mails to show
 */
function load_mailbox(mailbox) {

    // Show the mailbox and hide other views
    document.querySelector('#emails-view').style.display = 'block';
    document.querySelector('#compose-view').style.display = 'none';
    document.querySelector('#email-view').style.display = 'none';

    // Show the mailbox name
    document.querySelector('#emails-view').innerHTML = `<h3>${mailbox.charAt(0).toUpperCase() + mailbox.slice(1)}</h3>`;

    //request mails
    fetch(`/emails/${mailbox}`)
        .then(response => response.json())
        .then(emails => {
            show_mails(mailbox, emails)
        });
}

/**
 * Show a list of mails in an specific mailbox
 * @param mailbox name of the mailbox (allowed: inbox, sent, archived)
 * @param emails list of emails
 */
function show_mails(mailbox, emails) {
    emails.forEach(email => {
        const emailItem = document.createElement('div');
        if (email.read === true) {
            emailItem.className = 'email-item read';
        } else {
            emailItem.className = 'email-item';
        }

        emailItem.id = email.id;
        const email_user = mailbox === 'sent' ? email.recipients.join(", ") : email.sender
        emailItem.innerHTML = `
                <span class="email-user">${email_user}</span>
                <span class="email-subject">${email.subject}</span>
                <span class="email-timestamp">${email.timestamp}</span>
        `;
        emailItem.addEventListener('click', function () {
            load_mail(mailbox, this.id);
            set_read(this.id, true);
        });
        document.querySelector('#emails-view').append(emailItem);

    });

}

/**
 * Obtain an specific mail information
 * @param mailbox mailbox where the mail is showed
 * @param email_id id of the email
 */
function load_mail(mailbox, email_id) {
    document.querySelector('#emails-view').style.display = 'none';
    document.querySelector('#compose-view').style.display = 'none';
    document.querySelector('#email-view').style.display = 'block';

    fetch(`emails/${email_id}`)
        .then(response => {
            if (!response.ok) {
                return response.json()
                    .then(errorData => {
                        throw new Error('Response was not ok ' + errorData.error);
                    })
            }
            return response.json();
        })
        .then(email => {
            show_mail(mailbox, email);
        })
        .catch(error => {
            console.error('There has been a problem with your fetch operation:', error);
        });

}

/**
 * Show an specific mail information
 * @param mailbox mailbox where the email exist
 * @param email object that contains the email information
 */
function show_mail(mailbox, email) {
    const archiveButtonDisplay = mailbox === 'sent' ? 'none' : 'compact';
    const archived_button_text = email.archived ? "Unarchive" : "Archive"

    document.querySelector('#email-view').innerHTML = `
        <div><strong>From:</strong> ${email.sender}</div>
        <div><strong>To:</strong> ${email.recipients.join(", ")}</div>
        <div><strong>Subject:</strong> ${email.subject}</div>
        <div><strong>Timestamp:</strong> ${email.timestamp}</div>
        <button class="btn btn-sm btn-outline-primary" id="reply">Reply</button>
        <button class="btn btn-sm btn-outline-primary" id="archive" style="display: ${archiveButtonDisplay}" 
        onclick= "set_archived('${email.id}', ${!email.archived})">${archived_button_text}</button>
        <hr>
        <div class="text-wrap-custom">${email.body}</div>`

    document.querySelector('#reply').onclick = () => replyMail(email)
}

/**
 * Show the compose view with the correct fields for a email reply
 * @param email object that contains the email to replay information
 */
function replyMail(email) {
    const subject = email.subject.startsWith("Re:") ? email.subject : `Re: ${email.subject}`
    const body = `On ${email.timestamp} ${email.sender} wrote: \n${email.body}`
    compose_email(email.sender, subject, body)
}

/**
 * Send a mail with the data obtained from the compose form
 * @returns {boolean}
 */
function sendEmail() {
    const recipients = document.querySelector('#compose-recipients').value;
    const subject = document.querySelector('#compose-subject').value;
    const body = document.querySelector('#compose-body').value;

    fetch('/emails', {
        method: 'POST', headers: {
            'Content-Type': 'application/json'
        }, body: JSON.stringify({
            recipients: recipients, subject: subject, body: body
        })
    })
        .then(response => {
            if (!response.ok) {
                return response.json()
                    .then(errorData => {
                        throw new Error('Response was not ok ' + errorData.error);
                    })
            }
            return response.json();
        })
        .then(result => {
            console.log(result);
            load_mailbox('sent')
        })
        .catch(error => {
            console.error('There has been a problem with your fetch operation:', error);
        });

    return false;
}

/**
 * Set the read attribute of an email true or false
 * @param emailId {string} - email id
 * @param status {boolean} - parameter to indicate if the mail is read or not
 */
function set_read(emailId, status) {
    fetch(`/emails/${emailId}`, {
        method: 'PUT', body: JSON.stringify({
            'read': status
        })
    })
        .then(response => {
            if (!response.ok) {
                return response.json()
                    .then(errorData => {
                        throw new Error(errorData.error);
                    })
            }
        })
        .catch(error => {
            console.error('There has been a problem with your fetch operation:', error);
        });

}

/**
 * Set the archived attribute of an email to true or false
 * @param emailId {string} - email id
 * @param status {boolean} - parameter to indicate if the mail is read or not
 */
function set_archived(emailId, status) {
    fetch(`/emails/${emailId}`, {
        method: 'PUT', body: JSON.stringify({
            'archived': status
        })
    })
        .then(response => {
            if (!response.ok) {
                return response.json()
                    .then(errorData => {
                        throw new Error(errorData.error);
                    })
            }
            load_mailbox('inbox')
        })
        .catch(error => {
            console.error('There has been a problem with your fetch operation:', error);
        });

}