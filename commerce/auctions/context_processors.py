def watchlist_count(request):
    """
    Obtain the number of items in the watchlist for the user logged
    """
    if request.user.is_authenticated:
        return {'watchlist_count': len(request.user.watchlist.auctions.all())}
    else:
        return {'watchlist_count': ""}
