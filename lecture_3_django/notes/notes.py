"""
 -> Clients makes request to the server

REQUESTS EXAMPLE:

    GET / HTTP/1.1
    Host: www.example.com

    METHOD PAGE HTML_VERSION
    HOST

This will request the page / which is the root page with the version HTTP/1.1
and the host is the url
-------------------------------------------------------------------------------------
 -> Server response

 RESPONSE EXAMPLE:

    HTTP/1.1 200 OK
    Content-Type: text/html

    HTML_VERSION RESPONSE_CODE RESPONSE_VALUE
    CONTENT_TYPE: the response type

---------------------------------------------------------------------------------------
#############################
#   RESPONSE CODES
#############################

200     OK
301     Moved Permanently (you are moved from a web site to another)
403     Forbidden (you don't have access)
404     Not Found
500     Internal Server Error (Could be an error in the server)


#############################
#   DJANGO
#############################
It is a framework that allows us to only

#############################
#   Create a project with Django
#############################

Execute the command:
        django-admin startproject <name>

It will create a folder with the name passed and a file called manage.py

    - manage.py --> we don't need to touch it but can use it to execute commands
    - settings.py --> setting for our django app
    - urls.py   --> table of contents for our app urls

#############################
#   RUN DJANGO
#############################
Execute the command:
    python manage.py runserver

It says:

Starting development server at http://127.0.0.1:8000/

if we go to the url, django default page appears


#############################
#   CREATE AN APP
#############################
Execute the command:
        python manage.py startapp <name>

        example of name = hello

files generated:

    -view.py --> what is render to the user
    -app.py --> app for the user

"""


"""
CONFIGURATION

we created the app hello.

First we have to add it to our project "first_django_project" 
in the settings.py file, INSTALLED_APPS section

INSTALLED_APPS = [
    'hello',  # add our app to the INSTALLED APPS
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

then in the hello app, view.py we create some renders to the user

def index(request):
    return HttpResponse("Hello, world!")

this will response the user hello world
but we need to indicate which url has to access the user to get that response
for that we create an url.py for this app (hello) (the project has global urls.py but we want to 
separate things)

In urls.py for the app
    we define a urlpatterns = [] which contains all urls for this app
    
    to link the index view, we have to add an element
    
        - path("", views.index, name="index")
            . att1 = "": url, in this case it is empty
            . att2 = views.index: the view to link to the url
            . att3 = name: The name to that url pattern to reference from other part of the application

In urls.py for the project
    we need to include the urls.py from our app
    
    urlpatterns = [
        path('admin/', admin.site.urls),
        path('hello/', include("hello.urls"))
    ]
    
if we run the "python manage.py runserver"
we and enter in /hello, we will see the index with the phase "hello, world"

If we do any change, it is not needed to re launch de server

----------
We can add another url in the hello app, for example one that shows "hello, brian"

to access it we have to go to /hello/brian

==========================================
Define a parameter for a request
=========================================

If we want to say hello to multiple names, it is tedious to create an url for each of them
So we can pass an argument like "name" and we the page is executed we put the name after the hello

So if we call /hello/walter, the page will show "Hello, walter" 

def greet(request, name):
    return HttpResponse(f"Hello, {name}")
    
the function greet will be executed when we call the url /hello/something where something could be any string

==========================================
Render HTML Pages
=========================================
For printing Hello in a page we did:

def index(request):
    return HttpResponse("Hello")
    
But this can be very complicated if we want to show an entire HTML page.
For that we can render it passing the html page

def index(request):
    return render(request, "hello/index.html")
    
#
# CREATE THE HTML
#

A best practice is to create a folder called templates and inside it another folder with the app name

hello/templates/hello

and inside hello, the index.html

In this way we can call it using /hello/index.html and avoid confusions.


==========================================
Render HTML Pages with VARIABLES
=========================================
The function greet return an http response with the phase hello and the name passed.

def greet(request, name):
    return HttpResponse(f"Hello, {name.capitalize()}")

If we want to do this with an html file, django has some functionalities that allows to use variables in a html file

def greet(request, name):
    return render(request, "hello/greet.html", {
        "name": name
    })
    
then in the html file located in hello/templates/hello/greet.html we can use the variable name with {{ }}

<h1>Hello, {{ name }}!</h1>

#
# SUMMARY
#

In the project urls.py we include the urls of our app (hello)

    path('hello/', include("hello.urls"))
    
In the app urls.py we indicate that the views.greet will receive an string called name

    path("<str:name>", views.greet, name="greet"),

In View.py is decided which render should be shown based on the user request
In View.py for the hello app we create the view greet that receive a name as argument and render an html with it.

    def greet(request, name):
        return render(request, "hello/greet.html", {
            "name": name.capitalize()
        })

In the greet.html file we use the variable name

    <body>
        <h1>Hello, {{ name }}!</h1>
    </body>

==========================================
Render HTML Pages with CONDITIONALS
=========================================

## 1. CREATE APP

    python manage.py startapp newyear
    
## 2. ADD APP INTO PROJECT SETTINGS
    
    INSTALLED_APPS = [
        'hello',
        'newyear', 
        ...
    ]
    
## 3. ADD NEW APP URLS in PROJECT URLS

    urlpatterns = [
        path('admin/', admin.site.urls),
        path('hello/', include("hello.urls")),
        path('newyear/', include("newyear.urls"))
    ]

## 4. CREATE urls.py IN NEW APP AND ADD A NEW URL
    
    from django.urls import path
    from . import views
    
    urlpatterns = [
        path("", views.index, name="index")
    ]
    
## 5. CREATE A NEW VIEW IN newyear.views

    def index(request):
    now = datetime.datetime.now()
    return render(request, "newyear/index.html", {
        "newyear": now.month == 1 and now.day == 1
    })
    
    This view uses the index.html file that has to be created in app/templates/app 
    and in its context it have a variable "newyear" that will be true if it is new year
    
## 6. CREATE INDEX.HTML WITH CONDITIONS

    {% if newyear %}
        <h1>YES</h1>
    {% else %}
        <h1>NO</h1>
    {% endif %}
    
    A condition is formed by {% %} with the python conditions and it has to finish with {% endif %}
    

#########################################
STATIC FILES --> CSS
#########################################
Static files are files that do not change for example css files.
Previously we created an html file that changed based on the condition of if it is new year or not

To organize, inside the app create a new folder called

    Folder: static
    
And inside static create another folder with the app name

    Folder: static/newyear

And inside static/newyear place a new file for css --> styles.css

And inside styles.css we create some styles
    
    h1 {
        font-family: sans-serif;
        font-size: 90px;
        text-align: center;
    }

#########################################
APPLY CSS TO HTML
#########################################
Add {% load static %} to the top of the html file and
insert the <link> part in the head where the href is
{% static 'static_file_location' %}

and django will search where the file is.


    {% load static %}
    
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <link href="{% static 'newyear/styles.css' %}" rel="stylesheet">
        <title>Is it New Years's?</title>
    </head>



#########################################
FOR LOOP IN HTML (Tasks example)
#########################################

Create a new app called tasks (follow the newyear app steps)

this app will have a list of items that instead of add one by one
we will create a list in python and a for loop in html

1. View that passes a list as context
    tasks = ["foo", "bar", "baz"]


    def index(request):
        return render(request, "tasks/index.html", {
            "tasks": tasks
        })
    
2. Use the list tasks in html with a for loop

    <ul>
        {% for task in tasks %}
            <li>{{ task }}</li>
        {% endfor %}
    </ul>

3. If the list has no item and you want to show a message use --> {% empty %}

    {% for task in tasks %}
        <li>{{ task }}</li>
    {% empty %}
        <li>No tasks.</li>
    {% endfor %}

#########################################
FORM - create a new page html in the app
#########################################

1. Create a new html and a view for it

Add the path for views.add

    urlpatterns = [
        path("", views.index, name="index"),
        path("add", views.add, name="add") 
    ]



2. Create the view
    
    def add(request):
        return render(request, "tasks/add.html")

when the user calls tasks/add it will call the view views.add
and the view.add will open tasks/add.html

3. Create add.html

    <body>
        <h1>Add Task</h1>
        <form>
            <input type="text" name="task">
            <input type="submit">
        </form>
    </body>

#########################################
HTML LAYOUT
#########################################

when multiple pages share some part, we ca use a layout and then
extend from it

For example, we have the index.html and add.html in the app tasks that share the
head of the html. For that case we can create a new html file called layout and only 
write the shared code.

# layout.html
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Tasks</title>
    </head>
    <body>
      {% block body %}
      {% endblock %}
    </body>
    </html>

we define a new block inside the body that has to be replaced by each html page code
        {% block <name> %}
        {% endblock %}

# add.html

    {% extends "tasks/layout.html" %}

    {% block body %}
        <h1>Add Task</h1>
        <form>
            <input type="text" name="task">
            <input type="submit">
        </form>
    {% endblock %}

we add the {% extends <layout file> %} at the beggining

and the complete the blocks created in the layout
    {% block <name> %}
    {% endblock %}
    
#index.html
    
    {% extends "tasks/layout.html" %}

    {% block body %}
        <ul>
            {% for task in tasks %}
                <li>{{ task }}</li>
            {% endfor %}
        </ul>
    {% endblock %}

same for index.html


#########################################
HREF LINKS TO VIEWS
#########################################

We can reference links to the project pages by using the name

    <a href="{% url 'add' %}">Add a New Task</a>
    
use --> href="{% url 'name_of_the_view' %}"
where name_of_the_view is the name specified on urls.py --> path("add", views.add, name="add")

#########################################
SOLVE LINKS COLLISION
#########################################
when we have multiple apps that have the same url name, and we want to call it by href and the name
the page don't know which app are you referring to, so it just open the first one that find.

For example, the app "newyear" has the index.html called index 

    urlpatterns = [
        path("", views.index, name="index")
    ]

and the app "tasks" has the index.html called index
    
    urlpatterns = [
        path("", views.index, name="index"),
        path("add", views.add, name="add")
    ]

If we want to call from tasks/add to the index page by doing:
    <a href="{% url 'index' %}">View Tasks</a>
it will show the newyears view

SOLUTION:
add this in the tasks/urls.py 
    
    app_name = "tasks"
    
and then call the url by doing tasks:viewname

    <a href="{% url 'tasks:index' %}">View Tasks</a>
    
##########################################################
TOKEN TO PREVENT ATTACKS WHEN EXECUTING A POST --> CSRF
##########################################################

When a user executes a post by clicking the submit button, it is possible that could be an attack
if the page is not encrypted

To solve this, django provides something called csrf token that can be added easily to our form

For example, when pressing the submit button, django will try to go to tasks/add but it will give an error because
it is not encrypted

    <form action="{% url 'tasks:add' %}" method="post">
        <input type="text" name="task">
        <input type="submit">
    </form>
    
--> CSRF verification failed. Request aborted. <--

# SOLUTION:
Add {% csrf_token %} to the form
    
    <form action="{% url 'tasks:add' %}" method="post">
        {% csrf_token %}
        <input type="text" name="task">
        <input type="submit">
    </form>
    
##########################################################
CREATE FORMS WITH PYTHON
##########################################################
1. In views.py import the form library

    from django import forms
    
2. Create a new class that represent the form
    
    class NewTaskForm(forms.Form):
        task = forms.CharField(label="New Task")

3. Pass the class as context 

    def add(request):
        return render(request, "tasks/add.html", {
            "form": NewTaskForm()  # When form is used in add.html, a new NewTaskFrom() is created
        })
        
4. In add.html use the form passed as a context --> {{ form }}

    <form action="{% url 'tasks:add' %}" method="post">
        {% csrf_token %}
        {{ form }}
        <input type="submit">
    </form>
    
5. Add a new numeric input field to the form, it is only added to the class, the html will be updated because
it is using {{ form }} --> priority

    class NewTaskForm(forms.Form):
        task = forms.CharField(label="New Task")  # Create input field with a label
        priority = forms.IntegerField(label="Priority", min_value=1, max_value=10)
        
this input will have a min of 1 or max of 10, other values will have a client error.

##########################################################
HANDLE POST REQUESTS (client and server errors)
##########################################################

When user submit a form, this form is send to some endpoint.

For the case of this example, the endpoint is the same add page

when receiving the form, it can check if it is valid and execute actions (like add the new task to the list) 
or handle if there is an error, for example the server changed the priority limits but the user page was not updated
and had the old limits, so the server has to show an error:

# Add the handle for the post to the add view

    def add(request):

        #This is called every time someone uses the url tasks/add
    
        # When someone try to post to the add page, for example by submitting a form
        if request.method == "POST":
            form = NewTaskForm(request.POST)
            if form.is_valid():  # True if the form has no errors
                task = form.cleaned_data["task"]  # Obtain the value of the field task (same name that the class' item)
                tasks.append(task)  # add the new task to the global list
            else:
                # render the same form of the post to show errors
                # in form the field priority (or the one that have an error) has a new element called errorlist that 
                # will be shown
                return render(request, "tasks/add.html", {
                    "form": form
                })
        # RENDER FOR A NO POST REQUEST (GET IS BY DEFAULT)
        return render(request, "tasks/add.html", {
            "form": NewTaskForm()  # When form is used in add.html, a new NewTaskFrom() is created
        })
        
##########################################################
REDIRECT AFTER SUBMIT
##########################################################
When executing the previous code, we submit a new task, and if everything is ok it won't do anything for the user.
The new task will be added to the list but the user will continue seing the add page, not the list page.

To redirect to another page we can user HttpResponseRedirect(reverse("tasks:index"))
where reverse is a function that will create the url based on the name passed

        if form.is_valid():  # True if the form has no errors
            task = form.cleaned_data["task"]  # Obtain the value of the field task (same name that the class' item)
            tasks.append(task)  # add the new task to the global list
            return HttpResponseRedirect(reverse("tasks:index"))
            

##########################################################
SESSIONS
##########################################################

When creating list or storing data, this data is shown in the same way for the user in spain and for the user in 
china

To solve this problem, django provide sessions, where we can store data only for the computer using the web page.

In index for task/views.py check if the item "tasks" exist in the request.session list and create it.

    def index(request):
        # In sessions, we can store data that will be shown only for one computer
        if "tasks" not in request.session:
            request.session["tasks"] = []
        return render(request, "tasks/index.html", {
            "tasks": request.session["tasks"]
        })
        
This list is only available for one computer (based on cookies)

# MIGRATE

This will give an error because we first need to migrate the project to create the default tables, otherwise, it can
not be stored.

    python manage.py migrate (in a terminal)

# Store data

When a new task has to be store we can append as a normal list -> request.session["tasks"].append(task) 

    if form.is_valid():  # True if the form has no errors
        task = form.cleaned_data["task"]  # Obtain the value of the field task (same name that the class' item)
        request.session["tasks"] += [task]  # add the new task to the global list (do not use append)
        return HttpResponseRedirect(reverse("tasks:index"))

# Use data

To use the data store simply call request.session["tasks"]

    return render(request, "tasks/index.html", {
        "tasks": request.session["tasks"]
    })
        

"""
