from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import SET_NULL, SET
from django.db.models.signals import post_save
from django.dispatch import receiver


class User(AbstractUser):
    pass


class Category(models.Model):
    """
    Represent a Category for an auction
    """
    name = models.CharField(max_length=64)

    def __str__(self):
        return self.name


class Auction(models.Model):
    """
    Represent a item that to sell in the page
    """
    title = models.CharField(max_length=64)
    description = models.CharField(max_length=1024)
    starting_bid = models.IntegerField()
    picture = models.URLField(blank=True,
                              default="https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/"
                                      "No-Image-Placeholder.svg/1665px-No-Image-Placeholder.svg.png")
    active = models.BooleanField(default=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="auctions")
    category = models.ForeignKey(Category, blank=True, null=True, on_delete=SET_NULL, related_name="auctions")

    def __str__(self):
        return f"{self.title}"

    @property
    def actual_bid(self):
        """
        :return: the highest bid for the auction
        """
        return max(Bid.objects.filter(auction=self), key=lambda bid: bid.price)


class Bid(models.Model):
    """
    Represent a offer made from a user in an auction
    """
    price = models.IntegerField()
    auction = models.ForeignKey(Auction, on_delete=models.CASCADE, related_name="bids")
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="bids")

    def __str__(self):
        return f"{self.user} - {self.price}"


class Comment(models.Model):
    """
    Comments made on auction listings
    """
    text = models.CharField(max_length=128)
    date = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="comments")
    auction = models.ForeignKey(Auction, on_delete=models.CASCADE, related_name="comments")

    def __str__(self):
        return f"{self.text} - {self.user} - {self.auction}"


class Watchlist(models.Model):
    """
    A WatchList for each user to store the items that are interested in
    """
    user = models.OneToOneField(to=User,
                                on_delete=models.CASCADE,
                                related_name="watchlist",
                                primary_key=True)
    auctions = models.ManyToManyField(to=Auction, blank=True)

    def __str__(self):
        return f"{self.user} - {self.auctions}"


@receiver(post_save, sender=User)
def create_user_watchlist(sender, instance, created, **kwargs):
    """
    Signal handler to create a Watchlist for a user after user creation.
    """
    if created:
        Watchlist.objects.create(user=instance)
