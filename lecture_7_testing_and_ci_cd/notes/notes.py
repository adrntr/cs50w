"""
######################################################
#   TESTING
#####################################################

# ASSERT
Raise an exception is the assert is not correct

    assert square(10) == 100

######################################################
#   DJANGO testing for MODELS
#####################################################

In the file tests create a class that contains the tests for our models, for example flights



    from django.test import Client, TestCase

    from .models import Airport, Flight

    # Create your tests here.

    class FlightTestCase(TestCase):

        def setUp(self):
            # Create airports.
            a1 = Airport.objects.create(code="AAA", city="City A")
            a2 = Airport.objects.create(code="BBB", city="City B")

            # Create flights.
            Flight.objects.create(origin=a1, destination=a2, duration=100)
            Flight.objects.create(origin=a1, destination=a1, duration=200)
            Flight.objects.create(origin=a1, destination=a2, duration=-100)

        def test_departures_count(self):
            a = Airport.objects.get(code="AAA")
            self.assertEqual(a.departures.count(), 3)

        def test_arrivals_count(self):
            a = Airport.objects.get(code="AAA")
            self.assertEqual(a.arrivals.count(), 1)

        def test_valid_flight(self):
            a1 = Airport.objects.get(code="AAA")
            a2 = Airport.objects.get(code="BBB")
            f = Flight.objects.get(origin=a1, destination=a2, duration=100)
            self.assertTrue(f.is_valid_flight())

        def test_invalid_flight_destination(self):
            a1 = Airport.objects.get(code="AAA")
            f = Flight.objects.get(origin=a1, destination=a1)
            self.assertFalse(f.is_valid_flight())

        def test_invalid_flight_duration(self):
            a1 = Airport.objects.get(code="AAA")
            a2 = Airport.objects.get(code="BBB")
            f = Flight.objects.get(origin=a1, destination=a2, duration=-100)
            self.assertFalse(f.is_valid_flight())


In the setup we create fake data that will be deleted automatically after the tests had finished.

To run tests use:
-
    python3 manage.py test
-


######################################################
#   DJANGO testing for endpoints/index
#####################################################

    def test_index(self):
        c = Client()
        response = c.get('/flights/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["flights"].count(), 3)

In this test we create a client and do a get request to flights
Then we assert that the response is 200 and
using context that contains the information of the page, we assert that
the number of flights is 3


OTHER TESTS:

    def test_valid_flight_page(self):
        a1 = Airport.objects.get(code="AAA")
        f = Flight.objects.get(origin=a1, destination=a1)

        c = Client()
        response = c.get(f'/flights/{f.id}')
        self.assertEqual(response.status_code, 200)

    def test_invalid_flight_page(self):
        max_id = Flight.objects.all().aggregate(Max('id'))['id__max']

        c = Client()
        response = c.get(f'/flights/{max_id + 1}')
        self.assertEqual(response.status_code, 404)

    def test_flight_page_passengers(self):
        f = Flight.objects.get(pk=1)
        p = Passenger.objects.create(first="alice", last="Adam")
        f.passengers.add(p)

        c = Client()
        response = c.get(f'/flights/{f.id}')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["passengers"].count(), 1)

    def test_flight_page_non_passenger(self):
        f = Flight.objects.get(pk=1)
        p = Passenger.objects.create(first="alice", last="Adam")

        c = Client()
        response = c.get(f'/flights/{f.id}')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["non_passengers"].count(), 1)


#################################################################
#       SELENIUM
#################################################################

We can simulate a user interacting with the web, using a web driver.

Create a silly web to increase and decrease a number -> counter.html

Then create the test to execute automatically -> test.py

    import os
    import pathlib
    import unittest

    from selenium import webdriver


    def file_uri(filename):
        return pathlib.Path(os.path.abspath(filename)).as_uri()


    driver = webdriver.Chrome()


    class WebPageTests(unittest.TestCase):

        def test_title(self):
            driver.get(file_uri('counter.html'))
            self.assertEqual(driver.title, 'Counter')


Open a python terminal and execute the following (from the selenium folder)

This will open a chrome that is controlled by selenium
    >> from tests import *

Tell chrome to open the web
    >> uri = file_uri('counter.html')
    >> driver.get(uri)

Check some attributes as tittle or source
    >> driver.title
    >> driver.page_source

To test the plus button we need to get it first
(find_element can search by id by default but there are other options)
    >> increase = driver.find_element(value="increase")

To simulate user interaction (user press on increase button)
    >> increase.click()

We can do multiple clicks

    >> for i in range(25):
            increase.click()

for decrease

    >> decrease = driver.find_element(value="decrease")
    >> decrease.click()

####################################
# Example test.py
#####################################
    import os
    import pathlib
    import unittest

    from selenium import webdriver


    def file_uri(filename):
        return pathlib.Path(os.path.abspath(filename)).as_uri()


    driver = webdriver.Chrome()


    class WebPageTests(unittest.TestCase):

        def test_title(self):
            driver.get(file_uri('counter.html'))
            self.assertEqual(driver.title, 'Counter')

        def test_increase(self):
            driver.get(file_uri("counter.html"))
            increase = driver.find_element(value="increase")
            increase.click()
            self.assertEqual(driver.find_element(value="ache1").text, "1")

        def test_decrease(self):
            driver.get(file_uri("counter.html"))
            decrease = driver.find_element(value="decrease")
            decrease.click()
            self.assertEqual(driver.find_element(value="ache1").text, "-1")

        def test_multiple_increase(self):
            driver.get(file_uri("counter.html"))
            increase = driver.find_element(value="increase")
            for i in range(3):
                increase.click()
            self.assertEqual(driver.find_element(value="ache1").text, "3")


    if __name__ == '__main__':
        unittest.main()


# Another asserts

 - assertEqual
 - assertNotEqual
 - assertTrue
 - assertFalse
 - assertIn
 - assertNotIn


##############################################################
# CI/CD Github Actions
##############################################################

key1: value1
key2: value2
key3:
    - item1
    - item2
    - item3

# ci.yml

name: Testing
on: push

jobs:
    test_project:
        runs-on: ubuntu-latest
        steps:
        - uses: actions/checkout@v2
        - name: Run Django unit tests
          run: |
            pip3 install --user django
            python manage.py test

MIN: 1:09:12




"""
