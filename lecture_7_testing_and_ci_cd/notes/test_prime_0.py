from prime import is_prime


def test_primer(n, expected: bool):
    if is_prime(n) != expected:
        print(f"Error on is_primer({n}), expected {expected}")
