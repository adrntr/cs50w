
/* 
Single Page Applications

It means that a single page show multiple pages using javascript

See 01_single_page_application


####################################
#   Page to show different texts
####################################
This functions fetch the url from urls.py /sections/<int:num> 

    function showSections(section){
        fetch(`/sections/${section}`)
        .then(response => response.text())
        .then(text => {
            console.log(text);
            document.querySelector('#content').innerHTML = text;
        })
    }

In the view section the text to show is returned

    texts = ["text1", "text2", "text3"]

    def section(request, num):
        if 1 <= num <= 3:
            return HttpResponse(texts[num - 1])
        else:
            raise Http404("No such section")


####################################
#   CHANGING URL IN ONE PAGE APP
####################################
For one page app the url do not change, so the user loses the capacity to 
go back to the previous page

    history.pushState({section: section}, "", `section${section}`);

        arguments: (data, "", url to set)

To go back to previous page in one page app we have to use an event that pop the last
history state

    window.onpopstate = function(event) {
        // This is executed when user press back button
        console.log(event.state.section);
        showSection(event.state.section);
    }


#############################################
# window
############################################
window the pysical part that the user see
document represent the whole page, the part that the user sees and the part that the user do not see

Some properties

    - window.innerHeight    -->     The height of the window that the user see
    - window.innerWidth     -->     The width of the window that the user see
    - window.scrollY        -->     How many pixels the user scroll down the screen

    -document.body.offsetHeight     --> The full page height

Some calculations

    To know when the user reach the button of the page: 
        - window.scrollY + window.innerHeight = document.body.offsetHeight

        <script>
            window.onscroll = () =>{
                if(window.innerHeight + window.scrollY >= document.body.offsetHeight){
                    document.querySelector('body').style.background = 'green';
                }else{
                    document.querySelector('body').style.background = 'white';
                }
            }
        </script>

    This is useful for example for network as facebook to load more posts

#############################################
# Animation
############################################
css allows to animate 

## Example for making the text size bigger
    <style>
        @keyframes grow {
            from {
                font-size: 20px;
            }
            to {
                font-size: 100px;
            }
        }

        h1 {
            animation-name: grow;
            animation-duration: 2s;
            animation-fill-mode: forwards;
        }
    </style>

Define a style for h1 and set the propierties for animation (name, duration, fill-mode)
The name has to be defined as keyframes where you set the init style and the final style

## Example for moving the text 

    <style>
        @keyframes move {
            from {
                left: 0%
            }
            to {
                left: 50%
            }
        }

        h1 {
            position: relative
            animation-name: move;
            animation-duration: 2s;
            animation-fill-mode: forwards;
        }
    </style>

We can define different points of the animation with the percentage 

        @keyframes move {
            0%{
                left: 0%
            }
            50% {
                left: 50%
            }
            100% {
                left: 0%
            }
        }

        h1 {
            position: relative;
            animation-name: move;
            animation-duration: 2s;
            animation-fill-mode: forwards;
            animation-iteration-count: infinite;
        }

# animation-iteration-count --> How many times to reproduce the animation

################
# Control the animation with javascript
################
We can define a button and this button will start and stop the animation

- animationPlayState -> 'paused' or 'running'


    <script>
        document.addEventListener('DOMContentLoaded', function(){
            const h1 = document.querySelector('h1');
            h1.style.animationPlayState = 'paused';

            document.querySelector('button').onclick = () => {
                if(h1.style.animationPlayState == 'paused'){
                    h1.style.animationPlayState = 'running';
                } else {
                    h1.style.animationPlayState = 'paused';
                }
            }
        });
    </script>

################################
# Delete an element from list and animate it
################################
Create a list of "posts" and add a button to hide it

- animation-name: hide;             -> name of the animation
- animation-duration: 2s;           -> duration of the animation
- animation-fill-mode: forwards;    -> 
- animation-play-state: paused;     -> do not start the animation

        <style>
            @keyframes hide {
                0%{
                    opacity: 1;
                }
                100%{
                    opacity: 0;
                }
            }

            .post {
                background-color: #800e82;
                color: white; 
                padding: 20px;
                border-radius: 5px; 
                width: 200px; 
                text-align: center; 
                margin: 10 auto; 
                margin-bottom: 10px;
                animation-name: hide;
                animation-duration: 2s;
                animation-fill-mode: forwards;
                animation-play-state: paused;
            }
        </style>

- event.target                                                  -> return the element that had been clicked
- element.parentElement.style.animationPlayState = 'running';   -> active the animation
- element.parentElement.addEventListener('animationend',        -> detect when the animation finished
- element.parentElement.remove()                                -> Deleted the parent element that contains the element

        document.addEventListener('click', event => {
            element = event.target;
            if(element.className == "hide"){
                element.parentElement.style.animationPlayState = 'running';
                element.parentElement.addEventListener('animationend', ()=>{
                    element.parentElement.remove();
                })
            }
        })

        function add_post(contents){
            const post = document.createElement('div');
            post.className = 'post';
            post.innerHTML = `${contents} <button class="hide">Hide</button>`;

            document.querySelector('#posts').append(post)
        }

        
###################################################################################
#                                   REACT
###################################################################################
React is a library that helps to manage the user interface

Most programs --> Imperative programming

        VIEW: 
            <h1>0</h1>
        LOGIC
            let num = parseInt(document.querySelector("h1").innerHTML)
            num += 1;
            document.querySelector("h1").innerHTML = num;

React ->    It is Declarative Programming (the is no need to get the variable)
        VIEW: 
            <h1>{num}</h1>
        LOGIC
            num += 1;


Libraries needed: 
        React       -> define components and how they behave
        ReactDOM    -> takes react component and insert them into the DOM
        Babel       -> Translate code JSX (react) to Javascript, so all browser can understand this.

##########
# Example
###########

    <script type="text/babel">      # Indicate that the code is in JSX and need to be translated by babel
                                    # In real application this is done during deployment but here we do it on the fly
    
    </script>


    <body>
    <div id="app"></div>

    <script type="text/babel">
        function App(){
            //function that return a div with the result of the sum
            const x = 1;
            const y = 2;
            return (
                <div>{x + y}</div>
            );
        }

        // indicate to execute the App function and show the result in the div with the id=app
        ReactDOM.render(<App />, document.querySelector("#app"))  
    </script>
</body>

##################
# React to reuse components
#################
Instead of calling <h1>hello</h1> three times, we use the function Hello -> <Hello />

We can also send an attribute as name to the function Hello in order to print it.

<Hello name="Harry"/> -> Indicate the property

props -> to receive all properties

<h1>Hello, {props.name}</h1> -> To use the property with the name "name"


    <body>
        <div id="app"></div>

        <script type="text/babel">
            function Hello(props){
                return (
                    <h1>Hello, {props.name}</h1>
                );
            }
            function App(){
                return(
                    <div>
                        <Hello name="Harry"/>
                        <Hello name="Ron"/>
                        <Hello name="hermione"/>
                    </div>
                );
            }

            ReactDOM.render(<App />, document.querySelector("#app")) 
        </script>
    </body>

##################
# React to use variables
#################

we can define variables with -> React.useState(value) -> it return the variable and a function to change the value

    const [counter, setCount] = React.useState(0);

we can use this variable 
    
    <div>{counter}</div>

and change it value
    
    function updateCounter(){
        setCount(counter + 1);
    }

And this will be reflected in the page


    <body>
        <div id="app"></div>

        <script type="text/babel">

            function App(){

                const [counter, setCount] = React.useState(0);

                function updateCounter(){
                    setCount(counter + 1);
                }

                return(
                    <div>
                        <div>{counter}</div>
                        <button onClick={updateCounter}>Count</button>
                    </div>
                );
            }

            ReactDOM.render(<App />, document.querySelector("#app")) 
        </script>
    </body>

######################
# React app handling
#######################

When values are related we can define react variables as a dict
    
    const [state, setState] = React.useState({
        num1: 2,
        num2: 4,
        response: ""
    });

we can use these values

    <div>{state.num1} + {state.num2}</div>

also set these values.
To avoid setting all the dict again, we reuse with ...state, all values and just replace response

    setState({
        ...state,
        response: event.target.value
    })

to say that a value comes from an input we can use

    <input onChange={updateResponse} value={state.response} />

where the onChange has the function that update the state.response value and value set it

OnKeyPress --> Allows us to detect when a key is pressed over the input field and execute a function

    onKeyPress={inputKeyPressed}

if we only want to do something if the enter is pressed, in the function use:

    function inputKeyPressed(event){
        if(event.key === "Enter") {

to automatically set the cursor over an input use
    
    <input autoFocus={true}

--full script--

    <script type="text/babel">

        function App(){

            const [state, setState] = React.useState({
                num1: 2,
                num2: 4,
                response: "",
                score: 0
            });

            function inputKeyPressed(event){
                if(event.key === "Enter") {
                    const answer = parseInt(state.response)
                    if (state.num1 + state.num2 === answer){
                        setState({
                            ...state,
                            num1: Math.ceil(Math.random()*10),
                            num2: Math.ceil(Math.random()*10),
                            response: "",
                            score: state.score + 1
                        })
                    }else{
                        setState({
                            ...state,
                            response: "",
                            score: state.score -1
                        })
                    }
                }
            }

            function updateResponse(event){
                setState({
                    ...state,
                    response: event.target.value
                })
            }

            return (
                <div>
                    <div>{state.num1} + {state.num2}</div>
                    <input autoFocus={true} onKeyPress={inputKeyPressed} onChange={updateResponse} value={state.response} />
                    <div>Score: {state.score}</div>
                </div>
            )
        }

        ReactDOM.render(<App />, document.querySelector("#app")) 
    </script>

######################
# CSS and React
#######################
We can modify the style of a item using react and css
for example, set the class of an element based on a react variable

Example:
    The class "incorrect" can be set to an element based on the variable state.incorrect

    if the state.incorrect is false, the class is empty "", otherwise the class is "incorrect"

    <style>
        .incorrect{
            color: red;
        }
    </style>


    --

    const [state, setState] = React.useState({
        num1: 2,
        num2: 4,
        response: "",
        score: 0,
        incorrect: false
    });

    --

    <div id="problem" className={state.incorrect ? "incorrect" : ""}>{state.num1} + {state.num2}</div>


#######################
# Return another div in special cases
#######################
The function App is used to return a div, but we can return other div in special cases, for example, when
user reach a score

    if(state.score === 10){
        return(
            <div id="winner">
                You won!
            </div>
        )
    }

in this case the next return will not be used and the user will see a div that says "You won!"


*/