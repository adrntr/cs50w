from django.http import HttpResponseRedirect
from django.shortcuts import render
from django import forms
from django.urls import reverse


# Create your views here.

# tasks = ["foo", "bar", "baz"] -> Use request.session


# Create a new class for the form
class NewTaskForm(forms.Form):
    task = forms.CharField(label="New Task")  # Create input field with a label
    priority = forms.IntegerField(label="Priority", min_value=1, max_value=4)


def index(request):
    # In sessions, we can store data that will be shown only for one computer
    if "tasks" not in request.session:
        request.session["tasks"] = []
    return render(request, "tasks/index.html", {
        "tasks": request.session["tasks"]
    })


def add(request):
    """
    This is called every time someone uses the url tasks/add
    :param request:
    :return:

    """
    # When someone try to post to the add page, for example by submitting a form
    if request.method == "POST":
        form = NewTaskForm(request.POST)
        if form.is_valid():  # True if the form has no errors
            task = form.cleaned_data["task"]  # Obtain the value of the field task (same name that the class' item)
            request.session["tasks"] += [task]  # add the new task to the global list
            return HttpResponseRedirect(reverse("tasks:index"))  # reverse generates an url from a view name
                                                                #HttpResponseRedirect -> redirect user's browser to a different URL
        else:
            # render the same form of the post to show errors
            # in form the field priority (or the one that have an error) has a new element called errorlist that will
            # be shown
            return render(request, "tasks/add.html", {
                "form": form
            })
    return render(request, "tasks/add.html", {
        "form": NewTaskForm()  # When form is used in add.html, a new NewTaskFrom() is created
    })
